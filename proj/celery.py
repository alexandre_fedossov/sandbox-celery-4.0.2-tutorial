from __future__ import absolute_import, unicode_literals

import os

from celery import Celery

app = Celery('proj',
             broker=os.getenv('CELERY_BROKER', 'redis://127.0.0.1:6379/0'),
             backend=os.getenv('CELERY_RESULT_BACKEND', 'redis://127.0.0.1:6379/1'),
             include=['proj.tasks'])

# Optional configuration, see the application user guide.
app.conf.update(
    result_expires=3600,
    task_routes={
        'proj.tasks.step1': {'queue': 'step1'},
        'proj.tasks.step2_*': {'queue': 'step2'},
        'proj.tasks.step_final': {'queue': 'step_final'},
    }
)

if __name__ == '__main__':
    app.start()
