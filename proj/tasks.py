from celery import chain, group

from .celery import app


@app.task
def add(x, y):
    return x + y


@app.task
def step1(*args, **kwargs):
    return 'Step 1 Result'


@app.task
def step2_1(*args, **kwargs):
    return 'Step 2 Result'


@app.task
def step2_2(*args, **kwargs):
    return 'Step 2 Result'


@app.task
def step2_3(*args, **kwargs):
    return 'Step 2 Result'


@app.task
def step_final(*args, **kwargs):
    return 'Step 3 Result'


@app.task
def run_main_process(*args, **kwargs):
    chain(
        group(step1.s(), step1.s(), step1.s(), step1.s(), step1.s()),
        group(
            chain(step2_1.s(), step2_2.s(), step2_3.s()),
            chain(step2_1.s(), step2_2.s(), step2_3.s()),
            chain(step2_1.s(), step2_2.s(), step2_3.s()),
            chain(step2_1.s(), step2_2.s(), step2_3.s()),
            chain(step2_1.s(), step2_2.s(), step2_3.s()),
            chain(step2_1.s(), step2_2.s(), step2_3.s()),
        ),
        step_final.s(),
    ).apply_async()
