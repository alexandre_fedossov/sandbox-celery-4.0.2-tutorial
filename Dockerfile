FROM python:3
MAINTAINER Alexandre Fedossov <alexandre.fedossov@leverx.com>

RUN mkdir /app
WORKDIR /app

ADD requirements.txt requirements.txt
RUN pip install -r requirements.txt

ADD . .
CMD ["/sbin/init"]